﻿namespace MIDDLE_TEST.Models
{
    public class Transactions
    {
        public string Id { get; set; }
        public Employees Employee { get; set; }
        public Customer Customer { get; set; }
        public string Name { get; set; }

    }
}
