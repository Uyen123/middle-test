﻿namespace MIDDLE_TEST.Models
{
    public class Logs
    {
        public string Id { get; set; }
        public Transactions Transaction { get; set; }
        public string LoginDate { get; set; }
        public string LoginTime { get; set; }
    }
}
