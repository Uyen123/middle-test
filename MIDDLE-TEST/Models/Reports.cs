﻿namespace MIDDLE_TEST.Models
{
    public class Reports
    {
        public string Id { get; set; }
        public Accounts Account { get; set; }
        public Logs Logs { get; set; }
        public Transactions Transaction { get; set; }
        public string ReportName { get; set; }
        public string ReportDate { get; set; }
    }
}
