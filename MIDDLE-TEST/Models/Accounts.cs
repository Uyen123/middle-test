﻿namespace MIDDLE_TEST.Models
{
    public class Accounts
    {
        public string Id { get; set; }
        public string AccountName { get; set; }
        public Customer Customer { get; set; }
    }
}
